require 'tty-box'
require 'tty-screen'
require 'tty-prompt'
class YGTTYWrapper

  def initialize

    #session = SQLite3::Database.

    @stylebox = {
        fg: :bright_yellow,
        bg: :blue,
        border: {
            fg: :bright_yellow,
            bg: :blue
        }
    }

    @prompt = TTY::Prompt.new
  end


  # Different levels of boxes
  # -------------------------

  def book_box_this text
    box = TTY::Box.frame(width: TTY::Screen.width,
                         height: TTY::Screen.height,
                         align: :center,
                         style: @stylebox,
                         padding: [(TTY::Screen.height/2)-3,1 ]) do
      text
    end
    print_box_to_top box
  end

  def chapter_box_this text
    box = TTY::Box.frame(width: TTY::Screen.width-4, height: 5, align: :center, border: :thick, style: @stylebox, padding: 1) do
      text
    end
    print_box_to_top box
  end

  def header_1_box_this text
    box = TTY:: Box.frame(width: TTY::Screen.width-8, height: 5, align: :center, border: :light, style: @stylebox, padding: 1) do
      text
    end
    print_box_to_top box
  end

  def header_2_box_this text
    box = TTY:: Box.frame(width: TTY::Screen.width-12, height: 5, align: :center, border: :light, style: @stylebox, padding: 1) do
      text
    end
  end

  def header_3_box_this text
    box = TTY:: Box.frame(width: TTY::Screen.width-16, height: 5, align: :center, border: :light, padding: 1) do
      text
    end
    print_box_to_top box
  end

  def header_4_box_this text
    box = TTY:: Box.frame(width: TTY::Screen.width-20, height: 5, align: :center, border: :ascii, padding: 1) do
      text
    end
    print_box_to_top box
  end


  # Erase screen and put cursor to top
  def erase
    cursor = TTY::Cursor
    print cursor.clear_screen
    print cursor.up(TTY::Screen.height)
  end

  def print_box_to_top box
    erase
    print box
    sleep 1
  end

end

