require 'active_record'
require_relative './yg_tty_wrapper'
require_relative './models/micro'
require_relative './models/infra'


class ServiceDeclare < YGTTYWrapper


  def initialize
    super
    db_configuration_file = File.join(File.expand_path('..', __FILE__), '..', 'db', 'config.yml')
    db_configuration = YAML.load(File.read(db_configuration_file))
    ActiveRecord::Base.establish_connection(db_configuration['development'])
  end

  def call
    tables = ActiveRecord::Base.connection.tables
    p tables

    chapter_box_this "Service management"
    Micro.all.each do |micro|
      puts 'MICRO SERVICE : ' + micro.name
      puts '  - project_test_folder :' + micro.project_test_folder
      puts '  - git_address'
    end

    Infra.all.each do |infra|
      puts "INFRA SERVICE : " + infra.name
    end

    menu

  end

  def menu
    choice = @prompt.select("What do you want to do ?") do |menu|
      menu.choice 'Add an infra service (database,  redis, rabbitmq, ...)', :add_infra
      menu.choice 'Add a micro-service (usually, business logic oriented micro-services)', :add_micro
      menu.choice 'Remove an infra service', :remove_infra if Infra.count > 0
      menu.choice 'Remove a micro service',:remove_micro if Micro.count > 0
    end
    self.send choice
  end


  def add_infra
    infra_name = @prompt.ask('Give the name of the service : ')
    infra = Infra.new name: infra_name
    infra.save!
    puts infra.to_s
  end

  def add_micro
    micro={}
    micro[:name] = @prompt.ask "Give me the name of this service : "
    micro[:project_test_folder] = @prompt.ask "Where is the test folder containing the code of this project ?"
    micro[:git_address] = @prompt.ask "Give me the git address of that project (git@github.com/xxx.git) "
    micro_service = Micro.new name: micro[:name],
                              project_test_folder: micro[:project_test_folder],
                              git_address: micro[:git_address]

    micro_service.save!
    puts micro_service.to_s
  end

  def remove_infra
    choices = {}
    Infra.all.each_with_index do |infra,index|
      choices[:"#{index}"] = infra.id
    end
    choice = @prompt.select("Which infra service do you want to remove") do |menu|
      choices.each do |key,value|
        menu.choice "#{key} -- remove #{value}", key
      end
    end
    puts choices[choice]
    Infra.destroy(choices[choice])
  end

  def remove_micro
    choices = {}
    Micro.all.each_with_index { |micro,index | choices[:"#{index}"] = micro.id }
    choice =  @prompt.select("Which micro service do you want to remove") do |menu|
      choices.each do |key, value|
        menu.choice "#{key} -- remove #{value}", key
      end
    end
    Micro.destroy(choices[choice])
  end




end

test = ServiceDeclare.new
test.call
