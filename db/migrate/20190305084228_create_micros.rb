class CreateMicros < ActiveRecord::Migration[5.2]
  def change
    create_table :micros do |t|
      t.string :name
      t.string :project_test_folder
      t.string :git_address
    end
  end
end
